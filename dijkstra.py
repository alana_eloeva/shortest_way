# coding=utf-8
import json

from collections import defaultdict

file = open('graph.json', )
points = json.load(file)


def getPointByIndex(ind):
    for point in points:
        if point['index'] == ind:
            return point


# задаем входные значения
input_list = []

for point in points:
    for link in point['links']:
        start_point_index = point['index']
        dest_point_index = link['index']
        weight = link['weight']
        line = [start_point_index, dest_point_index, weight]
        input_list.append(line)

number_of_elements = len(points)
source = getPointByIndex(0)
target = getPointByIndex(6638)

# инициализируем граф
graph = defaultdict(list)
# заполняем граф: вершина -> (стоимость, вершина куда можем прийти)
for a, b, cost in input_list:
    graph[a] += [(cost, b)]

# инициализируем список вершин для посещения
nodes_to_visit = []
# добавляем исходную с расстоянием равным нулю
nodes_to_visit.append((0, source['index']))
# инициализируес список уникальных значений для хранения вершин которые уже посетили
visited = set()
# заполняем растояния до всех остальных вершин
# min_dist = {i: float('inf') for i in range(1, number_of_elements + 1)}
min_dist = {}
for point in points:
    min_dist[point['index']] = float('inf')
# заполняем расстояние до текущей вершины
min_dist[source['index']] = 0
# проходим по всем вершинам которые нужно посетить
# проходим до тех пор, пока такие вершины есть
while len(nodes_to_visit):
    # берем самую близкую вершину к нам
    # удаляем эту вершину из списка вершин для посещения
    cost, node = min(nodes_to_visit)
    # удаляем эту вершину из списка вершин для посещения
    nodes_to_visit.remove((cost, node))
    # проверяем что мы в нее еще не заходили
    if node in visited:
        continue
    # добавляем в список посещенных
    visited.add(node)
    # проходим по всем соединенным вершинам
    # n_cost - стоимость попадания из текущей вершины, n_node - прикрепленная вершина, в которую хотим попасть
    for n_cost, n_node in graph[node]:
        # проверяем нашли ли мы оптимальный путь
        if cost + n_cost < min_dist[n_node] and n_node not in visited:
            # если нашли то обновляем значение расстояния
            min_dist[n_node] = cost + n_cost
            # и добавляем эту вершину в спиок вершин для посещения
            nodes_to_visit.append((cost + n_cost, n_node))
            ss = "STOP"

min_weight = min_dist[6638]
# Выводим ответ
print(min_weight)

with open('dict1.txt', 'w') as file:
    file.write(json.dumps(min_dist))  # use `json.loads` to do the reverse
