# coding=utf-8
# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

# import folium
import json
import math
import random
import sys

import pandas as pd
from base64 import b64encode, b64decode
import pickle
import plotly.express as px
import plotly.graph_objects as go


from model import Point, Link, CEncoder


def distance(lat1, lon1, lat2, lon2):
    return math.hypot(lat2 - lat1, lon2 - lon1)
    # return sqrt((lat2 - lat1) ** 2 + (lon2 - lon1) ** 2)


file = open('Dorogi_Line.geojson', )
data = json.load(file)

list_lon = []
list_lat = []

list_lines = []

for i in data['features']:
    for line in i['geometry']['coordinates']:
        list_lines.append(line)
# for lon, lat in line:
#     list_lon.append(lon)
#     list_lat.append(lat)
# color = []
# for i in range(0, len(list_lines)):
#     # col = random.randint(0, sys.maxint)
#     for a in list_lines[i]:
#         list_lat.append(a[1])
#         list_lon.append(a[0])
#         # color.append(hex(col))
#     # color.append(None)
#     list_lat.append(None)
#     list_lon.append(None)
# # df = pd.DataFrame({"color": color, "lat": list_lat, "lon": list_lon})
# # print(df.head())
# # fig = px.scatter_mapbox(df, lat='lat', lon='lon', color='color')
# fig = go.Figure(go.Scattermapbox(mode='markers+lines', lon=list_lon, lat=list_lat, marker={'size': 5}))
# fig.update_layout(mapbox={'center': {'lon': 53.03466346, 'lat': 39.07714259},
#                           'style': 'stamen-terrain', 'center': {'lon': -20, 'lat': -20}, 'zoom': 5})
# fig.show()
# print (len(list_lon))


# ГРАФ
points = []


# проверка на повтор значения в массиве
def isDublicate(lat, lon):
    for point in points:
        if point.lat == lat and point.lon == lon:
            return True
        return False


def getPointIndex(lat, lon):
    for point in points:
        if point.lat == lat and point.lon == lon:
            return point.index
    return None


# добавление связей
def addLink(lat_main_point, lon_main_point, lat_linked_point, lon_linked_point):
    for point in points:
        if point.lat == lat_main_point and point.lon == lon_main_point:
            point.links.append(Link(index=getPointIndex(lat_linked_point, lon_linked_point),
                                    weight=distance(lat1=point.lat, lon1=point.lon,
                                                    lat2=lat_linked_point,
                                                    lon2=lon_linked_point)))



def removeLinkFromPointByIndex(point, link_index):
    for p in points:
        if p == point:
            for link in p.links:
                if link.index == link_index:
                    p.links.remove(link)


def addLinkToPoint(point, link):
    for p in points:
        if p == point:
            p.links.append(link)


def removeLinksFromPoint(point):
    for p in points:
        if p == point:
            p.links = []


def getPointByIndex(ind):
    for point in points:
        if point.index == ind:
            return point


# добавить все точки одного маршрута в массив (исключая повторы)
counter = 0
for i in range(len(list_lines)):
    for j in range(len(list_lines[i])):
        if len(points) != 0:
            if not isDublicate(list_lines[i][j][0], list_lines[i][j][1]):
                points.append(Point(index=counter, lat=list_lines[i][j][0], lon=list_lines[i][j][1]))
                counter += 1
        else:
            points.append(Point(index=counter, lat=list_lines[i][j][0], lon=list_lines[i][j][1]))
            counter += 1
    stopPoint = ""

# добавить связи
for i in range(len(list_lines)):
    for j in range(len(list_lines[i])):
        # проверить слева
        if j > 0:
            lat = list_lines[i][j - 1][0]
            lon = list_lines[i][j - 1][1]
            addLink(list_lines[i][j][0], list_lines[i][j][1], lat, lon)

        # проверить справа
        if len(list_lines[i]) > j + 1:
            lat = list_lines[i][j + 1][0]
            lon = list_lines[i][j + 1][1]
            addLink(list_lines[i][j][0], list_lines[i][j][1], lat, lon)

        stopPoint = ""
    print "add link", i

# # уменьшение
for i in range(len(points)):
    if len(points[i].links) == 2:
        # сумма весов
        sum = points[i].links[0].weight + points[i].links[1].weight

        lnk1 = Link(index=getPointByIndex(points[i].links[0].index).index, weight=sum)
        pnt1 = getPointByIndex(points[i].links[0].index)
        lnk2 = Link(index=getPointByIndex(points[i].links[1].index).index, weight=sum)
        pnt2 = getPointByIndex(points[i].links[1].index)

        removeLinkFromPointByIndex(pnt1, points[i].index)
        removeLinkFromPointByIndex(pnt2, points[i].index)

        pnt1.links.append(lnk2)
        pnt2.links.append(lnk1)

        points[i].links = []

        # points.insert(0, points.pop(i))
        print "processing", i

points_for_json = []

# удаление точек с пустыми линками
print(len(points))

for point in points:
    if len(point.links) > 0:
        points_for_json.append(point)

print(len(points_for_json))

from json import dumps, loads, JSONEncoder


# запись в файл
class CEncoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__


class PythonObjectEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (list, dict, str, int, float, bool, type(None))):
            return super(self).default(obj)
        elif isinstance(obj, set):
            return {"__set__": list(obj)}
        return {'_python_object': b64encode(pickle.dumps(obj)).decode('utf-8')}


def as_python_object(dct):
    if '__set__' in dct:
        return set(dct['__set__'])
    elif '_python_object' in dct:
        return pickle.loads(b64decode(dct['_python_object'].encode('utf-8')))
    return dct


json_string = CEncoder().encode(points_for_json).replace('\\', '')

with open("graph.json", "w") as file:
    file.write(json_string)
