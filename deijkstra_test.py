# coding=utf-8
import json

from collections import defaultdict

file = open('graph.json', )
points = json.load(file)


def getPointByIndex(ind):
    for point in points:
        if point['index'] == ind:
            return point


# задаем входные значения
input_list = []

for point in points:
    for link in point['links']:
        start_point_index = point['index']
        dest_point_index = link['index']
        weight = link['weight']
        line = [start_point_index, dest_point_index, weight]
        input_list.append(line)

number_of_elements = len(points)
source = getPointByIndex(0)
target = getPointByIndex(702)


class MinHeap:
    def __init__(self):
        self.list = [0]
        self.size = 0

    def move_up(self, i):
        while i // 2 > 0:
            if self.list[i] < self.list[i // 2]:
                self.list[i], self.list[i // 2] = self.list[i // 2], self.list[i]
            i = i // 2

    def insert(self, k):
        self.list.append(k)
        self.size += 1
        self.move_up(self.size)

    def move_down(self, i):
        while i * 2 <= self.size:
            mc = self.min_leaf(i)
            if self.list[i] > self.list[mc]:
                self.list[i], self.list[mc] = self.list[mc], self.list[i]
            i = mc

    def min_leaf(self, i):
        if i * 2 + 1 > self.size:
            return i * 2
        else:
            return (i * 2, i * 2 + 1)[self.list[i * 2] > self.list[i * 2 + 1]]

    def get_min(self):
        return_value = self.list[1]
        self.list[1] = self.list[self.size]
        self.size -= 1
        self.list.pop()
        self.move_down(1)
        return return_value


# инициализируем граф
graph = defaultdict(list)
# заполняем граф: вершина -> (стоимость, вершина куда можем прийти)
for a, b, cost in input_list:
    graph[a] += [(cost, b)]

# инициализируем список вершин для посещения
nodes_to_visit = MinHeap()
# добавляем исходную с расстоянием равным нулю
nodes_to_visit.insert((0, source['index']))
# Инициализируем список уникальных значений для хранения вершин которые уже посетили
visited = set()
# Заполняем расстояния до всех остальных вершие
# min_dist = {i: float('inf') for i in range(1, number_of_elements + 1)}
min_dist = {}
for point in points:
    min_dist[point['index']] = float('inf')
# Заполняем расстояние до текущей вершины
min_dist[source['index']] = 0
# Проходимся по всем вершинам которые нужно посетить
# Проходимся до тех пор, пока такие вершины есть
while nodes_to_visit.size:
    # Берем самую близкую вершину к нам
    # cost - стоимость попадания, node - название вершины
    cost, node = nodes_to_visit.get_min()
    # Проверяем что мы в нее еще не заходили (если вдруг мы сначала добавили (9,7), а потом (6,7)
    if node in visited:
        continue
    # Добавляем в список посещенных
    visited.add(node)
    # Проходимся по всем соединенным вершинам
    # n_cost - стоимость попадания из текущей вершины, n_node - прикрепленная вершина, в которую хотим попасть
    for n_cost, n_node in graph[node]:
        # Проверяем нашли ли мы оптимальный путь
        if cost + n_cost < min_dist[n_node] and n_node not in visited:
            # Если нашли то обновляем значение расстояния
            min_dist[n_node] = cost + n_cost
            # И добавляем эту вершину в список вершин для посещения
            nodes_to_visit.insert((cost + n_cost, n_node))

# Выводим ответ
min_weight = min_dist[702]
# Выводим ответ
print(min_weight)

# with open('dict2.txt', 'w') as file:
#     file.write(json.dumps(min_dist))
