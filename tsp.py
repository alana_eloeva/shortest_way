# КР путь

# конечная точка маршрута
import sys

from main import getPointByIndex

endPoint = getPointByIndex(19)

# список путей
listWays = []

# список весов для каждого путя(суммарное расстояние)
listResults = []

temp = []

# ближ точка к геопозиции пользователя
startPoint = getPointByIndex(0)

# добавляем в путь стартовую точку
temp.append(startPoint)

listWays.append(temp)
listResults.append(0)

indexDel = 0
while True:
    indexDel = 0
    minResult = sys.maxint
    for i in range(len(listResults)):
        if listResults[i] < minResult:
            minResult = listResults[i]
            indexDel = i
    temp = listWays[indexDel][:]
    indexEnd = temp[len(temp) - 1].index

    isFinish = True

    if endPoint.index != indexEnd:
        isFinish = False
    if isFinish:
        break

    newPoint = getPointByIndex(indexEnd)
    for link in newPoint.links:
        isOk = True
        for point in temp:
            if point.index == link.index:
                isOk = False

        if isOk:
            newWay = temp[:]
            newWay.append(getPointByIndex(link.index))
            listWays.append(newWay)
            listResults.append(minResult + link.weight)

    listWays.pop(indexDel)
    listResults.pop(indexDel)

# результат
temp = listWays[indexDel][:]
for way in temp:
    print way.index
