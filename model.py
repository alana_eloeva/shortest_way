import json


class Point(object):
    def __init__(self, index, lat, lon):
        self.index = index
        self.lat = lat
        self.lon = lon
        self.links = []

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

class CEncoder(json.JSONEncoder):
    def default(self, o):
        return o.__dict__

class Link(object):

    def __init__(self, index, weight):
        self.index = index
        self.weight = weight

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)


